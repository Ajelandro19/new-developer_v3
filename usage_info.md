# Usage Information

- **Last Used RTFM:** 
  - Year: 2024
  - Context: Utilized AxxonSoft documentation to integrate its API for a project.
- **Last Used LMGTFY:** 
  - Year: March 10, 2024
  - Context: Employed LMGTFY to swiftly address an Oauth2 and Google SignIn programming challenge.
- **Operating System:** Ubuntu Linux, Manjaro Linux or Windows (This project has been build using Ubuntu WSL on windows 11)
- **Languages Mastered:**
  - Python
  - JavaScript
  - Typescript
  - C++
  - Java
  - SQL
  - MongoDB