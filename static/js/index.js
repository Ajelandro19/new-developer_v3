document.addEventListener('DOMContentLoaded', () => {

    // Get all "navbar-burger" elements
    const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);
    // Add a click event on each of them
    $navbarBurgers.forEach( el => {
        el.addEventListener('click', () => {
            // Get the target from the "data-target" attribute
            const target = el.dataset.target;
            const $target = document.getElementById(target);
            console.log($target);
            // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
            el.classList.toggle('is-active');
            $target.classList.toggle('is-active');
        });
    });
});

function matchingPairs(){
    var numbers= document.getElementById('numbers-input').value.split(',').map(Number);
    var desiredSum= Number(document.getElementById('sum-input').value);

    jsonRequest = { 'array': numbers, 'desired_sum': desiredSum };
    fetch('/matching_pairs', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(jsonRequest)
    })
    .then(response => response.json())
    .then(data => {
        let number1 = data['number1'];
        let number2 = data['number2'];
        if (number1 === null && number2 === null){
            document.getElementById('result').innerHTML = `No matching pair found`;
            return;
        }
        document.getElementById('result').innerHTML = `The matching pair is: (${number1}, ${number2})`;
    });
}

function decodeText(){
    var text= document.getElementById('text-input').value;

    jsonRequest = { 'text': text };
    fetch('/data_decode', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(jsonRequest)
    })
    .then(response => response.json())
    .then(data => {
        let decodedText = data['result'];
        document.getElementById('result').innerHTML = `The decoded text is: ${decodedText}`;
    });
}