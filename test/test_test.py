import unittest
from base64 import b64encode

from scripts.convert_text import ConvertText

class ConvertTextTests(unittest.TestCase):
    def test_hex_to_string_valid_input(self):
        # Arrange
        text = "48656c6c6f20576f726c64"
        convert_text = ConvertText(text)

        # Act
        result = convert_text.hex_to_string()

        # Assert
        self.assertEqual(result, "Hello World")

    def test_hex_to_string_invalid_input(self):
        # Arrange
        text = "48656c6c6f20576f726c64Z"
        convert_text = ConvertText(text)

        # Act
        result = convert_text.hex_to_string()

        # Assert
        self.assertIsNone(result)

    def test_base64_to_string_invalid_input(self):
        # Arrange
        text = ("1G8FZ")
        convert_text = ConvertText(text)

        # Act
        result = convert_text.base64_to_string()
        print("result", result)

        # Assert
        self.assertIsNone(result)

    def test_base64_to_string_valid_input(self):
        # Arrange
        text = "SGVsbG8gV29ybGQ="
        convert_text = ConvertText(text)

        # Act
        result = convert_text.base64_to_string()

        # Assert
        self.assertEqual(result, "Hello World")

    def test_detect_and_convert_hex_input(self):
        # Arrange
        text = "48656c6c6f20576f726c64"
        convert_text = ConvertText(text)

        # Act
        result = convert_text.detect_and_convert()

        # Assert
        self.assertEqual(result, "Hello World")

    def test_detect_and_convert_unknown_format(self):
        # Arrange
        text = "SomeUnknownFormat"
        convert_text = ConvertText(text)

        # Act
        result = convert_text.detect_and_convert()

        # Assert
        self.assertEqual(result, "Formato desconocido")

if __name__ == "__main__":
    unittest.main()