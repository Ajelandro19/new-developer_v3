# Welcome to RebajaTusCuentas.com

Please complete this guide by uploading your work to your own gitlab repository 
and doing a MR to this one. The branch must contain the readme file with the
responses using markdown and referencing to folders or files
that you need in order to solve the test.


## 1

We encourage documentation and investigation in order to have agile development.
We support RTFM and LMGTFY:

[Link Text](usage_info.md)

## 2

Automation helps us to avoid human errors. Some of our systems use CI.

In this case, i use the gitlab CI tool to run pytest and build and push the project container if everything works fine. It's just and example because we would need to write our docker hub credentials.

[gitlabCI](.gitlab-ci.yml)


## 3


A developer's portfolio is important to us. We ask you to upload 1 or 2 
projects of your authorship.

[Intelimovil, just a fragment of code](/Intelimovil/)<br>
[Supervivencia al titanic](https://github.com/Mecuba/Supervivencia-en-el-Titanic/tree/Dise%C3%B1o_AppWeb)<br>
[Deployed supervivencia al titanic](https://titanicsurvival.azurewebsites.net/) It migth charge slow because it's deployed on a free plan



## 4

>___Please, write a code or pseudocode that solves the problem in which I have a 
collection of numbers in ascending order. You need to find the matching pair 
that it is equal to a sum that its also given to you. If you make any 
assumption, let us know.___

### [Code](/scripts/matching_pair.py)

## 5

"The message is in spanish."

### [Code](/scripts/convert_text.py)



# All answers must be inside a docker image and the answer will be tested with a running container. Add lint and at least 01 unit test

The container must run with 

> docker run -p 5000:5000  ajelandrogm1906/techtest:latest 

Pytest is running on docker build for practicity in this case.

It's not using gunicorn so you can see on console the answers to the questions, also you can check the webpage on browser with the linked showed by flask

Also, you can check the deployed page on this link <br>
# [Azure deploy](https://techtest-alejandro.azurewebsites.net/)