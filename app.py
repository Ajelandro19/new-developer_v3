"""
Este módulo contiene la aplicación para la prueba técnica
"""

import json
from flask import Flask, render_template, request, redirect, url_for
import markdown
import yaml
from scripts.matching_pair import MatchingPair
from scripts.convert_text import ConvertText
import requests
import threading 

app = Flask(__name__)

@app.route('/')
def index():
    """
    Función que renderiza el contenido de usage_info.md
    """
    with open('usage_info.md', 'r', encoding='utf-8') as f:
        content = f.read()
        html_content = markdown.markdown(content)
    return  render_template('index.html', content=html_content)
@app.route('/usage_info')
def usage_info():
    """
    Función que renderiza el contenido de usage_info.md
    """
    with open('usage_info.md', 'r', encoding='utf-8') as f:
        content = f.read()
    return content

@app.route('/ci', methods=['GET'])
def ci():
    """
    Función que renderiza el contenido de ci.md
    """
    with open('.gitlab-ci.yml', 'r', encoding='utf-8') as f:
        content = f.read()
    return content

@app.route('/ci_page', methods=['GET'])
def ci_page():
    """
    Función que renderiza el contenido de ci.md
    """
    with open('.gitlab-ci.yml', 'r', encoding='utf-8') as f:
        content = f.read()
        html_content = markdown.markdown(content)
    return render_template('ci.html', content=html_content)
        
@app.route('/data_decode', methods=['POST', 'GET'])
def convert():
    """
        Función que recibe un texto en formato JSON y lo convierte a un formato	
        diferente
    """
    if request.method == 'GET':
        return render_template('decoder.html')
    text = request.get_json()
    converter = ConvertText(text.get('text'))
    result = converter.detect_and_convert()
    return json.dumps({'result': result})

@app.route('/matching_pairs', methods=['POST', 'GET'])
def matching_pairs():
    """
       Recibe un arreglo de números y un número objetivo, y devuelve los dos números
    """
    if request.method == 'GET':
        return render_template('matching_pairs.html')
    data = request.get_json()
    process_request = MatchingPair(data.get('array'), data.get('desired_sum'))
    result = process_request.find_pair()
    if result is None:
        return json.dumps({'number1': None, 'number2': None})
    number1 = result[0]
    number2 = result[1]
    return json.dumps({'number1': number1, 'number2': number2})

@app.route('/projects')
def projects():
    """
        Función que renderiza el contenido de projects.html
    """
    return render_template('projects.html')

if __name__ == '__main__':
    threading.Thread(target=app.run, kwargs={'host': '0.0.0.0', 'port': 5000}).start()
    response=requests.get('http://0.0.0.0:5000/usage_info')
    print(response.text)
    response=requests.get('http://0.0.0.0:5000/ci')
    print(response.text)
    pair = MatchingPair([1, 2, 3, 4, 6], 6)
    print(pair.find_pair())
    decode = ConvertText("4573746520657320656c20fa6c74696d6f207061736f2c20706f72206661766f722c206167726567616d6520616c2068616e676f75743a200d0a0d0a226d617274696e406d656e646f7a6164656c736f6c61722e636f6d22207061726120736162657220717565206c6c656761737465206120657374612070617274652e0d0a0d0a477261636961732c20792065737065726f20766572746520706f7220617175ed212e")
    print(decode.detect_and_convert())
    decode = ConvertText("U2kgbGxlZ2FzIGhhc3RhIGVzdGEgcGFydGUsIHBvciBmYXZvciwgY29tZW50YW1lbG8gY29uIHVuIG1lbnNhamUu")
