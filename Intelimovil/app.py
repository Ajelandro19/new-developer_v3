from flask import Flask, render_template, Response, jsonify, request, redirect, url_for, session
from pythonfiles.tracking import movilai
from pythonfiles.registry import Registry
from pythonfiles.classcamera import Camera
from pythonfiles.pictures import set_passenger
import logging
import csv
import yaml
from datetime import datetime, timedelta
import datetime
import pytz
import socket
import os
import threading
import multiprocessing
import bcrypt
import sys

app = Flask(__name__, static_url_path='/static',
            static_folder='templates/static')
app.secret_key = 'secretkey'

passenger_type = None
count_flag = False
time_zone = pytz.timezone('America/Mexico_City')

# Initialize components
movilai = movilai()
registry = Registry()

# Restart the app
def restart_script():
    python_executable = sys.executable
    script_path = os.path.abspath(__file__)
    os.execv(python_executable, [python_executable, script_path] + sys.argv[1:])

# Function to run continuously in the background
def background_function():
    movilai.run()

# Start the background function in a separate thread
background_thread = threading.Thread(target=background_function)
background_thread.start()

# Validate the encrypted password
def validate_password(entered_password, stored_hashed_password):
    return bcrypt.checkpw(entered_password.encode('utf-8'), stored_hashed_password)

# Index route
@app.route('/', methods=['GET','POST'])
def index():
    if movilai.status:
        config_file_path = registry.password_path
        config_data = registry.read_file(config_file_path)
        stored_hashed_password = config_data['password']
        stored_username = config_data['username']

        if 'authenticated' in session:
            return redirect(url_for('process'))  # Redirect to /process route if already authenticated

        error_message = None

        if request.method == "POST":
            username = request.form.get("username")
            password = request.form.get("password")

            if username == stored_username and validate_password(password, stored_hashed_password):
                print("Authenticated")
                session['authenticated'] = True
                return redirect(url_for("process"))  # Redirect to /process route
            else:
                error_message = "Contraseña incorrecta. Intente de nuevo"
        return render_template("html/index.html", error_message=error_message)
    else:
        session['authenticated'] = True
        return render_template("html/first_config.html")

# Shows image without processing
@app.route('/monitor')
def monitor():
    if 'authenticated' not in session or not movilai.status:
        return redirect(url_for('index'))  # Redirect to login page if not authenticated
    return render_template('html/cam.html')

# Shows image with processing and data
@app.route('/process')
def process():
    if 'authenticated' not in session:
        return redirect(url_for('index'))  # Redirect to login page if not authenticated

    fecha, evento, subidas, bajadas, contador = registry.show_lastData("DELANTERO")
    if movilai.cameras_number == 2:
        fecha_back, evento_back, subidas_back, bajadas_back, contador_back = registry.show_lastData("TRASERO")
        return render_template('html/netcam.html', fecha=fecha, evento=evento, subidas=subidas, bajadas=bajadas, contador=contador, fecha_back=fecha_back, evento_back=evento_back, subidas_back=subidas_back, bajadas_back=bajadas_back, contador_back=contador_back)
    else:
        return render_template('html/netcam.html', fecha=fecha, evento=evento, subidas=subidas, bajadas=bajadas, contador=contador)

# Initialize camera withou processing
@app.route('/cam')
def cam():
    return Response(movilai.stream_camera('front'), mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route('/back_cam')
def back_cam():
    if movilai.cameras_number == 2:
        return Response(movilai.stream_camera('back'), mimetype='multipart/x-mixed-replace; boundary=frame')
    else:
        return False
# Initialize camera with processing
@app.route('/video_feed')
def video_feed():
    if 'authenticated' not in session:
        return redirect(url_for('index'))  # Redirect to login page if not authenticated

    return Response(movilai.stream_camera('main'), mimetype='multipart/x-mixed-replace; boundary=frame')


# Shows last data
@app.route('/update_data')
def update_data():
    fecha, evento, subidas, bajadas, contador = registry.show_lastData("DELANTERO")
    if movilai.cameras_number == 2:
        fecha_back, evento_back, subidas_back, bajadas_back, contador_back = registry.show_lastData("TRASERO")
        data = {'fecha': fecha, 'evento': str(evento),
        'subidas': str(subidas), 'bajadas': str(bajadas), 'contador': str(contador), 'fecha_back': fecha_back, 'evento_back': str(evento_back), 'subidas_back': str(subidas_back), 'bajadas_back': str(bajadas_back), 'contador_back': str(contador_back)}
    else:
        data = {'fecha': fecha, 'evento': str(evento),
            'subidas': str(subidas), 'bajadas': str(bajadas), 'contador': str(contador)}
    return jsonify(data)

@app.route('/ip_settings')
def ip_settings():
    if 'authenticated' not in session:
        return redirect(url_for('index'))  # Redirect to login page if not authenticated
    return render_template('html/config_cameras.html')

@app.route('/back_settings')
def back_settings():
    if movilai.cameras_number == 2:
        if 'authenticated' not in session:
            return redirect(url_for('index'))  # Redirect to login page if not authenticated

        return render_template('html/back_config.html')
    else:
        return redirect(url_for('index'))
    
@app.route('/set_credentials', methods=['POST'])
def set_credentials():
    config_file_path = registry.password_path
    if request.method == 'POST':
        json_data = request.get_json()
        username = json_data.get('username')
        password = json_data.get('password')
        config_data = {}
        config_data['username'] = username
        config_data['password'] = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt())
        registry.write_file(config_file_path, config_data)
        return jsonify({"success": True})
        
    else:
        return jsonify({"success": False})


@app.route('/validation', methods=['POST'])
def validation():
    if request.method =='POST':
        json_data = request.get_json()
        username = json_data.get('username')
        password = json_data.get('password')
        config_file_path = registry.password_path
        config_data = registry.read_file(config_file_path)
        stored_hashed_password = config_data['password']
        stored_username = config_data['username']
        if username == stored_username and validate_password(password, stored_hashed_password):
            return jsonify({"success": True})
        else:
            return jsonify({"success": False, "error": "Contraseña incorrecta. Intente de nuevo"})
    else:
        return jsonify({"success": False, "message": "POST method required"}) 

# Post form results to session and redirect to video_feed or cam route
@app.route('/submit-form/<string:many>', methods=['POST'])
def submit_form(many):
    if request.method == 'POST':
        file_path = registry.cameras_path
        json_data = request.get_json()
        if many == 'single':
            ip = json_data.get('ipcam')
            port = json_data.get('port')
            user = json_data.get('user')
            password = json_data.get('password')
            url = f"rtsp://{user}:{password}@{ip}:{port}/cam/realmonitor?channel=1&subtype=1"
            data = {
                'front_camera': {
                    'url': url
                },
                'back_camera': {
                    'url': None
                }
            }
        elif many == 'double':
            front_ip = json_data.get('front_ipcam')
            front_port = json_data.get('front_port')
            front_user = json_data.get('front_user')
            front_password = json_data.get('front_password')
            front_url = f"rtsp://{front_user}:{front_password}@{front_ip}:{front_port}/cam/realmonitor?channel=1&subtype=1"
            back_ip = json_data.get('back_ipcam')
            back_port = json_data.get('back_port')
            back_user = json_data.get('back_user')
            back_password = json_data.get('back_password')
            back_url = f"rtsp://{back_user}:{back_password}@{back_ip}:{back_port}/cam/realmonitor?channel=1&subtype=1"
            data = {
                'front_camera': {
                    'url': front_url
                },
                'back_camera': {
                    'url': back_url
                }
            }
        registry.write_file(file_path, data)
        return  restart_script()
    else:
        return redirect(url_for('static', filename='index.html'))


# Reset the count
@app.route('/reset', methods=['POST'])
def reset():
    try:
        registry.reset_count(movilai.cameras_number)
        movilai.update_count()
        if movilai.cameras_number == 1:
            front_count = movilai.get_counts()
            data = {"success":True, "front_count:": str(front_count) }
        if movilai.cameras_number == 2:
            front_count, back_count = movilai.get_counts()
            data = {"success":True, "front_count:": str(front_count), "back_count": str(back_count) }
        return jsonify(data)
    except Exception as e:
        print(e)
        return jsonify({"success": False, "message": str(e)})

# Shows the settings page
@app.route('/settings', methods=['GET', 'PUT']) 
def settings():
    if request.method == 'PUT':
        try:
            data = request.get_json()  # Get JSON data from the request body
            id = data.get('id')  
            roi = {
                'area': [int(data.get(f'area_{coord}')) for coord in ['x1', 'y1', 'x2', 'y2']],
                'roi': [int(data.get(f'roi_{coord}')) for coord in ['x1', 'y1', 'x2', 'y2']],
                'up': [int(data.get(f'up_{coord}')) for coord in ['x1', 'y1', 'x2', 'y2']],
                'down': [int(data.get(f'down_{coord}')) for coord in ['x1', 'y1', 'x2', 'y2']],
                'sensitivity': int(data.get('sensitivity'))
            }
            
            if str(id) == 'front':
                file_path = registry.front_roi_path
            elif str(id) == 'back':
                if movilai.cameras_number == 2:
                    file_path = registry.back_roi_path
                else:    
                    return jsonify({"success": False, "cameras_number": movilai.cameras_number})
            
            registry.write_file(file_path, {'roi': roi})
            movilai.set_ROI()
            
            return jsonify({"success": True, "roi": roi})
        except Exception as e:
            return jsonify({"success": False, "error": str(e)})
    return render_template('html/config.html')

# Gets the settings parameters from config.yaml
@app.route('/settings/<string:id>', methods=['GET'])
def get_settings(id):
    if request.method == 'GET':
        try:
            if str(id) == 'front':
                file_path = registry.front_roi_path
            elif str(id) == 'back':
                if movilai.cameras_number == 2:
                    file_path = registry.back_roi_path
                else:    
                    return jsonify({"success": False})
            config= registry.read_file(file_path)
            roi = config['roi']
            return jsonify(roi)
        except Exception as e:
            return jsonify({"success": False, "error": str(e)})
    else:
        return jsonify({"success": False})

# Set and get the passenger type which has get up 
@app.route('/pasajero', methods=['POST', 'GET'])
def process_json():
    global passenger_type
    if request.method == 'POST':
        from pythonfiles.pictures import get_count_flag
        json_data = request.get_json()
        clase = json_data['clase']
        tipo = json_data['tipo']
        boton = json_data['boton']
        proyect = json_data['proyect']
        ruta = json_data['ruta']
        unidad = json_data['unidad']
        ramal = json_data['ramal']
        tarifa_completo = json_data['tarifa_completo']
        tarifa_estudiante = json_data['tarifa_estudiante']
        tarifa_terceraEdad = json_data['tarifa_terceraEdad']
        tarifa_capacidadDif = json_data['tarifa_capacidadDif']
        fecha_hora = json_data['fecha_hora']
        pago = json_data['pago']
        completo = json_data['completo']
        estudiante = json_data['estudiante']
        terceraEdad = json_data['terceraEdad']
        capacidadDif = json_data['capacidadDif']
        completo_acumulado = json_data['completo_acumulado']
        estudiante_acumulado = json_data['estudiante_acumulado']
        terceraEdad_acumulado = json_data['terceraEdad_acumulado']
        capacidadDif_acumulado = json_data['capacidadDif_acumulado']
        conteoTotal = json_data['conteoTotal']
        dineroTotal = json_data['dineroTotal']
        # Process the JSON data here
        # You can perform any necessary operations or computations with the received data
        flag = get_count_flag()
        if flag:
            if int(estudiante) == 1:
                passenger_type = 'estudiante'
            elif int(terceraEdad) == 1:
                passenger_type = 'terceraEdad'
            elif int(capacidadDif) == 1:
                passenger_type = 'capacidadDif'
            elif int(completo) == 1:
                passenger_type = 'completo'
        # Return a response if needed
        return 'True'
    elif request.method == 'GET':
        if passenger_type is not None:
            temp_passenger = passenger_type
            passenger_type = None
            return temp_passenger
        else:
            return 'None'

@app.route('/cameras_number', methods=['GET'])
def cameras_number():
    return str(movilai.cameras_number)

@app.route('/device_settings', methods=['POST', 'PUT', 'GET'])
def update_device_settings():
    if request.method == 'GET':
            device_settings = registry.read_file(registry.device_settings_path)
            ruta = device_settings['ruta']
            unidad = device_settings['unidad']
            ramal = device_settings['ramal']
            proyect = device_settings['proyect']
            return jsonify({"success": True, "ruta": ruta, "unidad": unidad, "ramal": ramal, "proyect": proyect})
    try:
        json_data = request.get_json()
        ruta = json_data["ruta"].upper()
        unidad = json_data["unidad"].upper()
        ramal = json_data["ramal"].upper()
        proyect = json_data["proyect"].upper()
        clase = 'MOVILAI'
        today= datetime.date.today()
        try:
            prev_numero_serie = registry.read_file(registry.device_settings_path)['numero_serie']
        except:
            prev_numero_serie == 'serial_number'
        if request.method == 'PUT':
            if prev_numero_serie == 'serial_number':
                numero_serie = 'MA'+str(today.strftime('%d'))+str(today.strftime('%m'))+ruta[0:2]+proyect[-2:]+str(today.year)[-2:] 
            else:
                numero_serie = prev_numero_serie
        elif request.method == 'POST':
                numero_serie = 'MA'+str(today.strftime('%d'))+str(today.strftime('%m'))+ruta[0:2]+proyect[-2:]+str(today.year)[-2:] 
                
        registry.update_settings_parameters(numero_serie, ruta, unidad, ramal, clase, proyect)
        folder = registry.device_settings_path
        registry.write_file(folder, {'numero_serie': numero_serie, 'ruta': ruta, 'unidad': unidad, 'ramal': ramal, 'clase': clase, 'proyect': proyect})
        return jsonify({"success": True, "ruta": ruta, "unidad": unidad})
        
    except Exception as e:
        return jsonify({"success": False, "error": str(e)})

@app.route('/logout')
def logout():
    session.pop('authenticated', None)  # Remove authentication state from session
    return redirect(url_for('index'))

# Starts the app
if __name__ == '__main__':
    host = socket.gethostbyname(socket.gethostname())
    app.run(host="127.0.0.1", port=5000)
