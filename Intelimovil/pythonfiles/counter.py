from pythonfiles.registry import Registry
from pythonfiles.pictures import save_picture, set_count_flag
from datetime import datetime
from shapely.geometry import Point, box
import numpy as np
import jetson_utils as ju
import cv2
import Jetson.GPIO as GPIO
import time 
import threading
import multiprocessing
import pytz
import os
time_zone = pytz.timezone('America/Mexico_City')
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
led_pin=35
i=False
while i==False:
    try:
        GPIO.setup(led_pin, GPIO.OUT)
        i=True
    except:
        led_pin=35

GPIO.output(led_pin, GPIO.LOW)



class Counter:
    def __init__(self, camera_id):
        self.__camera_id = camera_id
        self.__temp_up_list = []
        self.__temp_down_list = []
        self.__up_intent = []
        self.__down_intent = []
        self.__registry = Registry()  
        self.__up_list, self.__down_list = self.__registry.update_data(self.__camera_id)
        self.__positions = {}
        self.__roi = (72, 53, 540, 417)
        self.__up_roi = (102, 92, 300, 399)
        self.__down_roi = (357, 94, 505, 394)

    #Define the counting area
    @property
    def roi(self):
        return self.__roi
    
    #Update the counting area
    @roi.setter
    def roi(self, new_roi):
        self.__roi = new_roi
    
    #Define the area of up direction
    @property
    def up_roi(self):
        return self.__up_roi
    
    #Update the area of up direction
    @up_roi.setter
    def up_roi(self, new_up_roi):
        self.__up_roi = new_up_roi
    
    #Define the area of down direction
    @property
    def down_roi(self):
        return self.__down_roi
    
    #Update the area of down direction
    @down_roi.setter
    def down_roi(self, new_down_roi):
        self.__down_roi = new_down_roi

    @property
    def up_list(self):
        return self.__up_list

    @up_list.setter
    def up_list(self, new_up_list):
        self.__up_list = new_up_list
    
    @property
    def down_list(self):
        return self.__down_list
    
    @down_list.setter
    def down_list(self, new_down_list):
        self.__down_list = new_down_list

    def turn_led(self, img, up_list, down_list, direction, counter):
        # Turn on the LED
        set_count_flag(True)
        GPIO.output(led_pin, GPIO.HIGH)
        time.sleep(.5)
        # Turn off the LED
        GPIO.output(led_pin, GPIO.LOW)
        time.sleep(.5)
        self.__registry.save_data(up_list, down_list, direction, counter)
        save_picture(img)

    def person_counter(self, box_id, img):
        id=box_id.TrackID
        index = 0
        up_roi = self.__up_roi
        down_roi = self.__down_roi
        # Define ROI polygons
        roi = box(self.__roi[0], self.__roi[1], self.__roi[2], self.__roi[3])
        up_roi_poly = box(up_roi[0], up_roi[1], up_roi[2], up_roi[3])
        down_roi_poly = box(down_roi[0], down_roi[1], down_roi[2], down_roi[3])
        
        # Find the center of the rectangle for detection
        ix, iy = box_id.Center
        if id not in self.__positions:
            self.__positions[id] = []
        # Find the current position of the vehicle
        if roi.contains(Point(ix, iy)):
            if up_roi_poly.contains(Point(ix, iy)):
                self.__positions[id].append(1)
            elif down_roi_poly.contains(Point(ix, iy)):
                self.__positions[id].append(2)
        elif (roi.contains(Point(ix, iy)) == False):
            keys_to_remove = []  # List to store keys to be removed
            if len(self.__positions) > 0:
                for id_key in self.__positions:
                    if len(self.__positions[id_key]) == 2:
                        if self.__positions[id_key] == [2, 1]:
                            self.__up_list[index] += 1
                            # Save the image using imwrite
                            img_np = ju.cudaToNumpy(img)
                            img_np = cv2.cvtColor(img_np, cv2.COLOR_RGBA2BGR)
                            try:
                                # p1 = multiprocessing.Process(target=turn_led, args = (img_np, self.__up_list, self.__down_list, 'Up', self.__camera.id))
                                # p1.start() 
                                background_thread = threading.Thread(target=self.turn_led, args = (img_np, self.__up_list, self.__down_list, 'Up', self.__camera_id))
                                background_thread.start()
                            except Exception as e:
                                print("Error in save_data", e)
                            keys_to_remove.append(id_key)
                        elif self.__positions[id_key] == [1, 2]:
                            self.__down_list[index] += 1
                            # Save the image using imwrite
                            try:
                                save_register = threading.Thread(target=self.__registry.save_data, args=(self.__up_list, self.__down_list, 'Down', self.__camera_id))
                                save_register.start()
                                # save_register = multiprocessing.Process(target=save_data, args=(self.__up_list, self.__down_list, 'Down', self.__camera.id))
                                # save_register.start()
                                #save_data(self.__up_list, self.__down_list, 'Down', self.__camera.id)
                            except Exception as e:
                                print("Error in save_data", e)
                            keys_to_remove.append(id_key)
                    elif len(self.__positions[id_key]) == 3:
                        if self.__positions[id_key] == [2, 1, 1] or self.__positions[id_key] == [2, 2, 1]:
                            self.__up_list[index] += 1
                            # Save the image using imwrite
                            img_np = ju.cudaToNumpy(img)
                            img_np = cv2.cvtColor(img_np, cv2.COLOR_RGBA2BGR)
                            try:
                                background_thread = threading.Thread(target=self.turn_led, args = (img_np, self.__up_list, self.__down_list, 'Up', self.__camera_id))
                                background_thread.start()
                                # p2 = multiprocessing.Process(target=turn_led, args=(img_np, self.__up_list, self.__down_list, 'Up', self.__camera.id))
                                # p2.start()
                            except Exception as e:
                                print("Error in save_data", e) 
                            keys_to_remove.append(id_key)
                        elif self.__positions[id_key] == [1, 1, 2] or self.__positions[id_key] == [1, 2, 2]:
                            self.__down_list[index] += 1
                            try:
                                save_register2 = threading.Thread(target=self.__registry.save_data, args=(self.__up_list, self.__down_list, 'Down', self.__camera_id))
                                save_register2.start()
                                # save_register2 = multiprocessing.Process(target=save_data, args=(self.__up_list, self.__down_list, 'Down', self.__camera.id))
                                # save_register2.start()
                                #save_data(self.__up_list, self.__down_list, 'Down', self.__camera.id)
                            except Exception as e:
                                print("Error in save_data", e)
                            keys_to_remove.append(id_key)
                    elif len(self.__positions[id_key]) == 0:
                        keys_to_remove.append(id_key)
                # Remove the keys from positions dictionary
                for key in keys_to_remove:
                    self.__positions.pop(key)
        if id in self.__positions and len(self.__positions[id]) > 3:
            self.__positions[id] = [self.__positions[id][0], self.__positions[id][1], self.__positions[id][-1]]
        
