import pandas as pd
from datetime import datetime
from pythonfiles.parameters import MovilParamaters
import json
import csv
import pytz
import os
import requests
import multiprocessing
import yaml
import time

class Registry:
    def __init__(self):
        self.__time_zone = pytz.timezone('America/Mexico_City')
        self.__data_path = '/Projects/InteliMovil/data'
        os.makedirs(self.__data_path, exist_ok=True)  # This will create the directory if it doesn't exist
        #Paths to app files
        self.__cameras_path = os.path.join(self.__data_path, '.cameraurl.yaml')
        self.__device_settings_path = os.path.join(self.__data_path, '.device_settings.yaml')
        self.__registry_path = os.path.join(self.__data_path, 'registry.csv')
        self.__failed_post_path = os.path.join(self.__data_path, '.failed_post.txt')
        self.__front_roi_path = os.path.join(self.__data_path, '.config.yaml')
        self.__back_roi_path = os.path.join(self.__data_path, '.back_config.yaml')
        self.__password_path = os.path.join(self.__data_path, '.configpass.yaml')
        self.__ip_path = os.path.join(self.__data_path, '.ip.yaml')
        self.__MAX_RETRIES = 3
        self.__registry_url = "http://trainz-api.accesa.me/disp_train/registro"

        #Initialize the device settings parameters
        if not self.check_file_exist(self.__device_settings_path):
            self.__settings = MovilParamaters("serial_number", "Accesa", "3", "Accesa", "MOVILAI", "M17")
            settings = {'numero_serie': self.__settings.serial_number, # 'serial_number': 'serial_number
                'ruta': self.__settings.route,
                    'unidad': self.__settings.unit,
                    'ramal': self.__settings.ramal,
                    'clase': self.__settings.class_id,
                    'proyect': self.__settings.project}
            self.write_file(self.__device_settings_path, settings)
        else:
            data = self.read_file(self.__device_settings_path)
            self.__settings = MovilParamaters(data['numero_serie'], data['ruta'], data['unidad'], data['ramal'], data['clase'], data['proyect'])
    
    @property
    def cameras_path(self):
        return self.__cameras_path

    @property
    def front_roi_path(self):
        return self.__front_roi_path
    
    @property
    def back_roi_path(self):
        return self.__back_roi_path

    @property
    def password_path(self):
        return self.__password_path
    
    @property
    def device_settings_path(self):
        return self.__device_settings_path
        
    def check_file_exist(self, file_path):
        return os.path.exists(file_path)

    def write_file(self, file_path, data: dict):
        with open(file_path, 'w') as file:
            yaml.dump(data, file)

    def read_file(self, file_path):
        with open(file_path, 'r') as file:
            data = file.read()
        return yaml.safe_load(data)

    def get_settings(self):
        return self.__settings
    
    #Function to get the cameras url
    def get_cameras_number(self):
        if check_file_exist(self.__cameras_path):
            data = self.read_file(self.__cameras_path)
            front_camera = config['front_camera']
            front_url = front_camera['url']
            back_camera = config['back_camera']
            back_url = back_camera['url']
            if back_url == None:
                self.__cameras_number = 1
            elif front_url == None:
                self.__status = False
            else:
                self.__cameras_number = 2
            return 

    #Function to update the device settings
    def update_settings_parameters(self, serial_number, ruta, unidad, ramal, clase, proyect):
        self.__settings.serial_number = serial_number
        self.__settings.route = ruta
        self.__settings.unit = unidad
        self.__settings.ramal = ramal
        self.__settings.class_id = clase
        self.__settings.project = proyect

    #Function to check the internet connection
    def check_internet_connection(self):
        try:
        # Try to send a request to Google to check if internet is accessible
            response = requests.get("https://www.google.com", timeout=5)
            response.raise_for_status()
            return True
        except requests.exceptions.ConnectionError as e:
            print("No internet connection available.")
            return False
        except requests.exceptions.Timeout as e:
            print("Internet connection timed out.")
            return False
        except requests.exceptions.RequestException as e:
            print("There was an ambiguous exception that occurred while handling your request.")
            return False
        except request.exceptions.HTTPError as e:
            print("HTTP Error")
            return False
    
    #Function to get the registries payload to send to the API
    def get_payload(self, up_list, down_list, direction, counter):
        payload = {
        "numero_serie": self.__settings.serial_number,
        "ruta": self.__settings.route,
        "unidad": self.__settings.unit,
        "ramal": self.__settings.ramal,
        "clase": self.__settings.class_id,
        "proyect": self.__settings.project,
        "id_dispositivo": counter,
        "status": "1",
        "tipo": "REGISTRO_CONTEO",
        "evento": direction,
        "fecha_hora": datetime.now(self.__time_zone).strftime("%Y-%m-%d %H:%M:%S"),
        "cuenta_total_subidas" : str(int(up_list[0])),
        "cuenta_total_bajadas" : str(int(down_list[0]))
        }
        return json.dumps(payload)

    #Function to post the data registries to the API, if there is no internet connection, the data is saved in a file to be sent later
    def post_data(self, up_list, down_list, direction, counter):
        new_payload = self.get_payload(up_list, down_list, direction, counter)
        # Specify the URL to post the JSON data
        if self.check_internet_connection():
            payloads = self.get_failed_payloads()
            payloads.append(new_payload)
            failed_payloads = []
            for json_payload in payloads:
                # Specify the URL to post the JSON data
                json_payload.strip()
                for retry in range(self.__MAX_RETRIES):
                    url = self.__registry_url
                    # Set the Content-Type header to indicate JSON data
                    headers = {"Content-Type": "application/json"}
                    try:
                        # Send the POST request with the JSON payload
                        response = requests.post(url, data=json_payload, headers=headers)
                        # Check the response status code
                        if response.status_code == 201:
                            break
                        else:
                            print("Failed to post JSON data. Status code:", response.status_code)
                            if retry < self.__MAX_RETRIES - 1:
                                print("Retrying...")
                            else:
                                self.save_payload(json_payload)
                    except  requests.RequestException as e:
                        print("Error sending the request:", e)
                        self.save_payload(json_payload)
        else:
            print("No internet connection")
            self.save_payload(new_payload)

    #Function to get the failed payloads to send to the API saved in a file
    def get_failed_payloads(self):
        file_path = self.__failed_post_path
        failed_payloads = []
        if not self.check_file_exist(file_path):
            return failed_payloads
        with open(file_path, "r") as file:
            failed_payloads = file.readlines()
        with open(file_path, "w") as file:
            file.write("")
        if not failed_payloads:
            return []
        return failed_payloads

    #Function to save the failed payloads in a file when there is no internet connection
    def save_payload(self, payload):
        file_path = self.__failed_post_path
        folder = os.path.dirname(file_path)
        if not os.path.exists(folder):
            os.makedirs(folder)
        with open(file_path, "a") as file:
                file.write(payload.strip() + "\n")

    #Function to send the up and down values to the main code
    def get_ordered_dataframe(self, counter):
        df = pd.read_csv(self.__registry_path)
        df.sort_values(by='Fecha', ascending=True, inplace=True)
        df = df[df['Contador'] == counter.upper()]
        df = df.iloc[[-1]]
        return df
    
    #Function to get the up and down values to the main code
    def update_data(self, counter : str):
        try:
            df = self.get_ordered_dataframe(counter)
            # Initialize empty arrays for subidas and bajadas
            subidas = []
            bajadas = []
            # Append the values to the arrays
            subidas.append(df['Subidas'].values[0])
            bajadas.append(df['Bajadas'].values[0])
            return subidas, bajadas
        except:
            return [0], [0]
    #Function to save the data in the csv file
    def save_data(self, up_list, down_list, direction : str, counter : str):
        now = datetime.now(self.__time_zone)
        current_date = now.strftime("%Y-%m-%d")
        current_time = now.strftime("%H:%M:%S")
        df = pd.read_csv(self.__registry_path)
        conteos = pd.DataFrame({'Fecha': [current_date+' '+current_time],
                                'Contador': [counter.upper()],
                                'Direccion': [direction],
                                'Subidas': up_list[0],
                                'Bajadas':down_list[0]})
        conteos_general = pd.concat([df, conteos])
        # conteos_general['Fecha']=pd.to_datetime(conteos_general['Fecha']).dt.normalize()
        conteos_general.drop_duplicates(
            subset=['Fecha', 'Contador'], keep='last', inplace=True)
        conteos_general.to_csv(self.__registry_path, index=False)
        self.post_data(up_list, down_list, direction, counter)

    #function to send the last data to the front end
    def show_lastData(self, counter : str):
        df = self.get_ordered_dataframe(counter.upper())
        fecha = df['Fecha'].values[0]
        evento = df['Direccion'].values[0]
        contador = df['Contador'].values[0]
        try:
            subidas=(df['Subidas'].values[0])
            bajadas=(df['Bajadas'].values[0])
        except:
            subidas = 0
            bajadas = 0
        return fecha, evento, subidas, bajadas, contador

    #Function to check the last date in the csv file and reset the counting if the date is different from today
    def check_last_date(self, cameras_number):
        if self.check_file_exist(self.__registry_path):
            df = pd.read_csv(self.__registry_path)
            df.sort_values(by='Fecha', ascending=True, inplace=True)
            df = df.iloc[[-1]]
            fecha = df['Fecha'].values[0] 
            fecha_without_time = fecha.split(' ')[0]
            today = datetime.now(self.__time_zone).strftime("%Y-%m-%d")
            if fecha_without_time != today:
                print("Se reinicia el conteo++++++++++++++++++++++++++++++++++++++")
                self.reset_count(cameras_number)
        else:
            print("El archivo no existe1111111111111111111111111111111111111111111111111111")
            with open(self.__registry_path, 'w', newline='', encoding='utf-8-sig') as f:
                    writer = csv.writer(f)
                    writer.writerow(["Fecha", "Contador", "Direccion", "Subidas", "Bajadas"])
            self.reset_count(2)

    #Function to reset the counting
    def reset_count(self, cameras_number):
        now = datetime.now(self.__time_zone)
        current_date = now.strftime("%Y-%m-%d")
        current_time = now.strftime("%H:%M:%S")
        if cameras_number == 2:
            contadores = ['DELANTERO', 'TRASERO']
        if cameras_number ==1:
            contadores = ['DELANTERO']
        for counter in contadores:
            df = pd.read_csv(self.__registry_path)
            conteos = pd.DataFrame({'Fecha': [current_date+' '+current_time],
                                    'Contador': [counter],
                                    'Direccion': ['RESET'],
                                    'Subidas': 0,
                                    'Bajadas': 0})
            conteos_general = pd.concat([df, conteos])
            conteos_general.drop_duplicates(
                    subset=['Fecha', 'Contador'], keep='last', inplace=True)
            conteos_general.to_csv(self.__registry_path, index=False)
            # conteos_general['Fecha']=pd.to_datetime(conteos_general['Fecha']).dt.normalize()
            fecha, evento, subidas, bajadas, contador = self.show_lastData(counter)
            self.post_data([0], [0], "RESET", counter)
        print("Reset Complete")
        return True, fecha, evento, subidas, bajadas

    #Function to patch device settings on database  
    def patch_device_settings(self):
        settings = self.get_settings()
        ip = self.read_file(self.__ip_path)
        print("IP:", ip)
        payload = {
            "clase": "MOVILAI",
            "tipo": "firmware",
            "numero_serie": settings.serial_number,
            "ruta": settings.route,
            "unidad": settings.unit,
            "ramal": settings.ramal,
            "proyect": settings.project,
            "ip": ip
        }
        url = "http://trainz-api.accesa.me/disp_train/status"
        headers = {"Content-Type": "application/json"}
        response = requests.patch(url, data=json.dumps(payload), headers=headers)
        if response.status_code == 200:
            print("Device settings updated successfully")
            return True
        else:
            print("Failed to update device settings. Status code:", response.status_code)
            return False