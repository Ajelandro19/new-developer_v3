import jetson_inference as ji
import jetson_utils as ju
from pythonfiles.counter import Counter
import time
import cv2
import Jetson.GPIO as GPIO
class Camera:
    #Initialize the class
    def __init__(self, url, pin, id):
        self.__led = pin
        #Set up the GPIO
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.__led, GPIO.OUT)
        GPIO.output(self.__led, GPIO.LOW)
        #Set up the camera
        self._cap = None
        self.__reconnecting = False 
        self.__status = False
        self.__url=url #/dev/video0
        image_path = "templates/images/black.jpg"
        jpg_img = cv2.imread(image_path)
        self.__img = jpg_img
        self.__id = id
        #Set up the neural network
        INFERENCE_THRESHOLD = 0.3
        self.net = ji.detectNet('ssd-mobilenet-v2', ['--log-level=error'], threshold=INFERENCE_THRESHOLD)
        self.net.SetTrackingEnabled(True)
        self.__aroi = (0, 0, 640, 480)
        #Set up the counter
        self.__counter = Counter(self.__id)


    #Define the area of interest
    @property
    def aroi(self):
        return self.__aroi
    
    #Update the area of interest
    @aroi.setter
    def aroi(self, new_aroi):
        self.__aroi = new_aroi
    
    #Define the camera id
    @property
    def id(self):
        return self.__id

    #Update the camera id
    @id.setter
    def id(self, new_id):
        self.__id = new_id
    
    #Define the camera image
    @property
    def img(self):
        return self.__img
    
    #Update the camera image
    @img.setter
    def img(self, new_img):
        self.__img = new_img
    
    @property
    def url(self):
        return self.__url

    @url.setter
    def url(self, new_url):
        self.__url = new_url

    @property
    def status(self):
        return self.__status
    
    @status.setter
    def status(self, new_status):
        self.__status = new_status

    @property
    def reconnecting(self):
        return self.__reconnecting
    
    @reconnecting.setter
    def reconnecting(self, new_reconnecting):
        self.__reconnecting = new_reconnecting

    @property
    def counter_up_list(self):
        return self.__counter.up_list
    
    @counter_up_list.setter
    def counter_up_list(self, new_list):
        self.__counter.up_list = new_list
    
    @property
    def counter_down_list(self):
        return self.__counter.down_list
    
    @counter_down_list.setter
    def counter_down_list(self, new_list):
        self.__counter.down_list = new_list
    
    @property
    def counter_roi(self):
        return self.__counter.roi
    
    @counter_roi.setter
    def counter_roi(self, new_roi):
        self.__counter.roi = new_roi
    
    @property
    def counter_up_roi(self):
        return self.__counter.up_roi
    
    @counter_up_roi.setter
    def counter_up_roi(self, new_roi):
        self.__counter.up_roi = new_roi
    
    @property
    def counter_down_roi(self):
        return self.__counter.down_roi

    @counter_down_roi.setter
    def counter_down_roi(self, new_roi):
        self.__counter.down_roi = new_roi

    def person_counter(self, detected, img):
        self.__counter.person_counter(detected, img)

    #Stablish the capture device 
    def connect_camera(self):
        try:
            self._cap = ju.videoSource(self.__url, argv=['--input-width=640', '--input-height=480'])
            self.turn_on_led()
            self.__reconnecting = False
            self.__status = True
        except Exception as e:
            print("Failed to open camera")
            print(str(e))

    #Disconnect the capture device
    def disconnect_camera(self):
        self.turn_off_led()
        self.__status = False
        if self._cap is not None:
            try:
                self._cap.Close()
                self._cap = None
            except Exception as e:
                print("Failed to close camera")
                print(str(e))

    #Restart the capture device
    def restart_camera(self):
        while not self.__status:
            image_path = "templates/images/black.jpg"
            jpg_img = cv2.imread(image_path)
            self.__img = jpg_img
            self.__reconnecting = True
            try:
                self.disconnect_camera()
                time.sleep(5)
                self.connect_camera()
                if self.__status:
                    break
                else:
                    time.sleep(10)
            except Exception as e:
                print(f"An error occurred while reconnecting {camera}: {str(e)}")
                time.sleep(20)  # Add a delay before the next reconnection attempt
    
    def turn_on_led(self):
        GPIO.output(self.__led, GPIO.HIGH)
    
    def turn_off_led(self):
        GPIO.output(self.__led, GPIO.LOW)