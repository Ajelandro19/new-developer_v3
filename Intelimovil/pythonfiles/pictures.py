import cv2
from datetime import datetime
import os
import pytz
import time
import threading
import requests
time_zone = pytz.timezone('America/Mexico_City')
passenger = None
last_passenger = None
passenger_lock = threading.Lock()
count_flag = False 
count_flag_lock = threading.Lock()
flag_file = "/Projects/InteliMovil/data/.count_flag.txt"

#Flag to receive the passenger id
def set_count_flag(flag):
    global flag_file
    with open(flag_file, 'w') as file:
        file.write(str(int(flag)))

def get_count_flag():
    global flag_file
    with open(flag_file, 'r') as file:
        flag = bool(int(file.read().strip()))
    return flag

#Function to set the passenger id
def set_passenger(id):
    global passenger, last_passenger
    last_passenger = passenger
    passenger = id

def get_passenger():
    response = requests.get('http://127.0.0.1:5000/pasajero')
    if response.status_code == 200:
        if response.text == "None":
            return None
        else:
            return response.text
    else:
        return None

#Function to save the picture
def save_picture(img, direction):
    global passenger, last_passenger
    now = datetime.now(time_zone)
    date = now.strftime("%Y%m%d")
    hour = now.strftime("%H%M%S")
    passenger_id = None
    start_time = time.time()
    if direction == "Up":
        set_count_flag(True)
        #Wait for the passenger id for 15 seconds or until the passenger id is received.
        #If the passenger id is not received, the picture is saved in the folder "sin_registro"
        while time.time() - start_time < 15:
            if passenger_id is None:
                passenger_id = get_passenger()
                time.sleep(1)
            if passenger_id is not None:
                break
    if passenger_id is not None:
        folder=f"/Projects/InteliMovil/pictures/{date}/{direction}/{passenger_id}"
    else:
        folder=f"/Projects/InteliMovil/pictures/{date}/{direction}/sin_registro"
    os.makedirs(folder, exist_ok=True)
    cv2.imwrite(f"{folder}/{hour}.jpg", img)
    set_count_flag(False)


