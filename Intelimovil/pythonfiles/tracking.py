import jetson_inference as ji
import jetson_utils as ju
import numpy as np
from pythonfiles.classcamera import Camera
from pythonfiles.counter import Counter
from pythonfiles.registry import Registry
import cv2
import time
import multiprocessing
from datetime import datetime
import threading
import requests
import pytz
import json
import os
import yaml


class movilai:
    def __init__(self):
        self.__output = ju.videoOutput('rtsp://127.0.0.1:5005/cam_output', '--output-latency=500')
        self.__status = False
        self.__time_zone = pytz.timezone('America/Mexico_City')
        self.__cameras = []
        self.__status = False
        self.__registry= Registry()
        self.__cameras_number = 0
    @property
    def status(self):
        return self.__status
    
    @status.setter
    def status(self, new_status):
        self.__status = new_status

    @property
    def cameras_number (self):
        return self.__cameras_number
    
    def get_counts(self):
        if self.__cameras_number == 1:
            return self.__front_camera.counter_up_list[0]
        elif self.__cameras_number == 2:
            return self.__front_camera.counter_up_list[0], self.__back_camera.counter_up_list[0]

    #Run the movilai counter function
    def run(self):
        try:
            file = self.__registry.cameras_path
            #If there's a file with the cameras configuration the app can run, if not, the app can't run and will wait for the user to set the fisrt configuration
            if self.__registry.check_file_exist(file):
                self.__status = True
            
            #Config the counter parameters
            config = self.__registry.read_file(file)
            front_camera = config['front_camera']
            front_url = front_camera['url']
            back_camera = config['back_camera']
            back_url = back_camera['url']
            #Set the number of cameras
            if back_url == None:
                self.__cameras_number = 1
            elif front_url == None:
                self.__status = False
            else:
                self.__cameras_number = 2
            #Start the cameras neural network
            if self.__cameras_number == 1:
                self.__front_camera = Camera(front_url, 33, "DELANTERO")
                self.__cameras.append("front_camera")
                count_thread = threading.Thread(target=self.real_time, args=("front_camera",))
                count_thread.start()
            if self.__cameras_number == 2:
                self.__front_camera = Camera(front_url, 33, "DELANTERO")
                self.__back_camera = Camera(back_url, 37, "TRASERO")
                self.__cameras.append("front_camera")
                self.__cameras.append("back_camera")
                count_thread = threading.Thread(target=self.real_time, args=("front_camera",))
                count_thread.start()
                count_thread2 = threading.Thread(target=self.real_time, args=("back_camera",))
                count_thread2.start()
            self.__registry.patch_device_settings()
            self.__registry.check_last_date(self.__cameras_number)
            self.update_count()
            self.set_ROI()
            patch_thread = threading.Thread(target=self.patch_realtime_data)
            patch_thread.start()
            while True:
                #Start streaming server
                self.combine_images()
        except Exception as e:
            print(e)
            self.__status = False

    #Update the counter 
    def update_count(self):
        for num in range(len(self.__cameras)):
            if self.__cameras[num] == 'front_camera':
                self.__front_camera.counter_up_list, self.__front_camera.counter_down_list = self.__registry.update_data(self.__front_camera.id)
            if self.__cameras[num] == 'back_camera':
                self.__back_camera.counter_up_list, self.__back_camera.counter_down_list = self.__registry.update_data(self.__back_camera.id)
        print("Actualizando conteo")

    def get_ROI(self, file):
        path = file
        if self.__registry.check_file_exist(path):
            config = self.__registry.read_file(path)
            return config
        else:
            return None
    
    #Set the ROI for the cameras
    def set_ROI(self):
        for num in range(len(self.__cameras)):
            try:
                if self.__cameras[num] == 'front_camera':
                    config = self.get_ROI(self.__registry.front_roi_path)
                    if config != None:
                        front_camera = config['roi']
                        self.__front_camera.aroi = front_camera['area']
                        self.__front_camera.counter_roi = front_camera['roi']
                        self.__front_camera.counter_up_roi = front_camera['up']
                        self.__front_camera.counter_down_roi = front_camera['down']
                    else:
                        roi = {
                            'area': [int(coord) for coord in self.__front_camera.aroi],
                            'roi': [int(coord) for coord in self.__front_camera.counter_roi],
                            'up': [int(coord) for coord in self.__front_camera.counter_up_roi],
                            'down': [int(coord) for coord in self.__front_camera.counter_down_roi],
                            'sensitivity': 100
                        }
                        self.__registry.write_file(self.__registry.front_roi_path, {'roi': roi})
                        
                if self.__cameras[num] == 'back_camera':
                    file = self.__registry.back_roi_path
                    config = self.get_ROI(self.__registry.back_roi_path)
                    if config != None:
                        back_camera = config['roi']
                        self.__back_camera.aroi = back_camera['area']
                        self.__back_camera.counter_roi = back_camera['roi']
                        self.__back_camera.counter_up_roi = back_camera['up']
                        self.__back_camera.counter_down_roi = back_camera['down']
                    else:
                        roi = {
                            'area': [int(coord) for coord in self.__back_camera.aroi],
                            'roi': [int(coord) for coord in self.__back_camera.counter_roi],
                            'up': [int(coord) for coord in self.__back_camera.counter_up_roi],
                            'down': [int(coord) for coord in self.__back_camera.counter_down_roi],
                            'sensitivity': 100
                        }
                        self.__registry.write_file(self.__registry.back_roi_path, {'roi': roi})
            except Exception as e:
                print("El archivo no exixte, tomando valores predeterminados")
    
    #Counter main function
    def real_time(self, cam):

        def draw_roi(img, roi, color, thickness=1):
            x1, y1, x2, y2 = roi
            ju.cudaDrawLine(img, (x1, y1), (x2, y1), color, thickness)
            ju.cudaDrawLine(img, (x2, y1), (x2, y2), color, thickness)
            ju.cudaDrawLine(img, (x2, y2), (x1, y2), color, thickness)
            ju.cudaDrawLine(img, (x1, y2), (x1, y1), color, thickness)

        if cam == 'front_camera':
            camera = self.__front_camera
        if cam == 'back_camera':
            camera = self.__back_camera

        camera.turn_off_led()
        #Restarts the camera connection
        camera.connect_camera()
        font = ju.cudaFont()

        while True:
            #Capture the image from the first camera
            if camera.status:
                try:
                    #Capture and transform the image before sending it to the neural network
                    img = camera._cap.Capture()
                    img_cropped = ju.cudaAllocMapped(
                        width=camera.aroi[2] - camera.aroi[0],
                        height=camera.aroi[3] - camera.aroi[1],
                        format=img.format
                    )
                    ju.cudaCrop(img, img_cropped, camera.aroi)

                    #Detect objects in the image
                    detections = camera.net.Detect(img_cropped, overlay='none')

                    #Draw the bounding boxes for the detections
                    for detected in detections:
                        camera.person_counter(detected, img)
                        draw_roi(img, detected.ROI, (243, 83, 49, 200), 1)
                        ju.cudaDrawCircle(img, detected.Center, 2, (255, 0, 0, 200))

                    draw_roi(img, camera.aroi, (255, 0, 0, 200))
                    draw_roi(img, camera.counter_roi, (0, 255, 0, 200))
                    draw_roi(img, camera.counter_up_roi, (255, 0, 255, 200))
                    draw_roi(img, camera.counter_down_roi, (0, 0, 255, 200))
                    #Updating the image
                    camera.img = ju.cudaToNumpy(img)
                except Exception as e:
                    print(f"An error occurred with camera: {str(e)}")
                    camera.status = False
                    camera.restart_camera()
            elif not camera.status:
                if camera.reconnecting == False:
                    camera.reconnecting = True 
                    camera.restart_camera()

    #Convert image to CUDA format and send it to the streaming server          
    def combine_images(self):
        if len(self.__cameras) == 1:
            combined_img = ju.cudaFromNumpy(self.__front_camera.img)
        if len(self.__cameras) == 2:
            combined_img = ju.cudaFromNumpy(np.hstack((self.__front_camera.img, self.__back_camera.img)))
        self.__output.Render(combined_img)

    #Stream the camera to the page
    def stream_camera(self, view):
        if str(view)== 'front':
            url = self.__front_camera.url
        elif str(view) == 'back':
            url= self.__back_camera.url
        elif str(view) == 'main':
            url = 'rtsp://127.0.0.1:5005/cam_output'

        cap = cv2.VideoCapture(url)
        if not cap.isOpened():
            print("Error: VideoCapture could not be opened.")
        else:
            # Set codec and bitrate (adjust as needed)
            desired_fps = 12  # Set the desired frame rate (e.g., 10 FPS)
            cap.set(cv2.CAP_PROP_FPS, desired_fps)
            fourcc = cv2.VideoWriter_fourcc(*'HEVC')
            cap.set(cv2.CAP_PROP_FOURCC, fourcc)
            cap.set(cv2.CAP_PROP_BITRATE, 1000)  # Set bitrate in bps (100 Kbps)

        while True:
            #Read frame from the camera
            ret, frame = cap.read()
            reconnect_delay = 5
            #Check if frame read successfully
            if not ret:
                cap.release()  # Release the current capture
                time.sleep(reconnect_delay)  # Wait for the specified delay before reconnecting
                cap = cv2.VideoCapture(url)  # Reconnect the camera
                continue
            if self.__cameras_number == 2 and str(view) == 'main':
                frame = cv2.resize(frame, (1280, 480))
            else:
                frame = cv2.resize(frame, (640, 480))
            frame = cv2.imencode('.jpg', frame)[1].tobytes()
            yield (b'--frame\r\n' b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')
        cap.release()

    #Send the last data to the API each minute
    def patch_realtime_data(self):
        MAX_RETRIES = 3
        #Specify the URL to post the JSON data
        while True:
            if self.__registry.check_internet_connection():
                settings = self.__registry.get_settings()
                temp = os.popen("cat /sys/devices/virtual/thermal/thermal_zone0/temp").read()
                if self.__cameras_number == 1:
                    contadores = ['DELANTERO']
                elif self.__cameras_number == 2:
                    contadores = ['DELANTERO', 'TRASERO']
                for counter in contadores:
                    fecha, direction, subidas, bajadas, contador = self.__registry.show_lastData(counter)
                    if counter == 'DELANTERO':
                        status=self.__front_camera.status
                        serial_number=settings.serial_number+'D'
                    if counter == 'TRASERO':
                        status=self.__back_camera.status
                        serial_number=settings.serial_number+'T'
                    payload = {
                    "numero_serie": serial_number,
                    "ruta": settings.route,
                    "unidad": settings.unit,
                    "ramal": settings.ramal,
                    "clase": settings.class_id,
                    "proyect": settings.project,
                    "id_dispositivo": contador,
                    "status_camara": str(status),
                    "tipo": "TIEMPO_REAL",
                    "evento": direction,
                    "fecha_hora": datetime.now(self.__time_zone).strftime("%Y-%m-%d %H:%M:%S"),
                    "cuenta_total_subidas" : str(subidas),
                    "cuenta_total_bajadas" : str(bajadas),
                    "temp": str(int(temp)/1000),
                    }
                    json_payload = json.dumps(payload)

                    #Specify the URL to post the JSON data
                    for retry in range(MAX_RETRIES):
                        url = "http://trainz-api.accesa.me/disp_train/status"

                        #Set the Content-Type header to indicate JSON data
                        headers = {"Content-Type": "application/json"}
                        try:
                            #Send the POST request with the JSON payload
                            response = requests.patch(url, data=json_payload, headers=headers)

                            #Check the response status code
                            if response.status_code == 200 or response.status_code == 201 or response.status_code == 500:
                                break
                            else:
                                print("Failed to post JSON data. Status code:", response.status_code)
                                if retry < MAX_RETRIES - 1:
                                    print("Retrying...")
                        except  requests.RequestException as e:
                            print("Error sending the request:", e)
            time.sleep(60)


