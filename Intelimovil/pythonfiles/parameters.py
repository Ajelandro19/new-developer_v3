class MovilParamaters:
    def __init__(self, serial_number, route, unit, ramal, class_id, project):
        self.__serial_number = serial_number
        self.__route = route
        self.__unit = unit
        self.__ramal = ramal
        self.__class_id = class_id
        self.__project = project

    @property
    def serial_number(self):
        return self.__serial_number
    
    @serial_number.setter
    def serial_number(self, new_serial_number):
        self.__serial_number = new_serial_number
    
    @property
    def route(self):
        return self.__route

    @route.setter
    def route(self, new_route):
        self.__route = new_route
    
    @property
    def unit(self):
        return self.__unit
    
    @unit.setter
    def unit(self, new_unit):
        self.__unit = new_unit
    
    @property
    def ramal(self):
        return self.__ramal
    
    @ramal.setter
    def ramal(self, new_ramal):
        self.__ramal = new_ramal

    @property
    def class_id(self):
        return self.__class_id
    
    @class_id.setter
    def class_id(self, new_class_id):
        self.__class_id = new_class_id
    
    @property
    def project(self):
        return self.__project
    
    @project.setter
    def project(self, new_project):
        self.__project = new_project