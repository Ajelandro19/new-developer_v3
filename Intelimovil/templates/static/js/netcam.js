let cameraCount;

        // Use Fetch API to make an AJAX request to the Flask API endpoint
        fetch('/cameras_number')
            .then(response => response.json())
            .then(data => {
                // Access the camera count from the JSON response
                cameraCount = parseInt(data);
                // Now, you can use the cameraCount variable in other scripts
                // For example, you can log it to the console
            })
            .catch(error => {
                console.error('Error fetching camera count:', error);
            });

// Function to reset the counter
function resetCount() {
  var user = prompt("Ingrese usuario: ");
  var password = prompt("Ingrese contraseña: ");
  var request = {
    username: user,
    password: password,
  };
  fetch("/validation", {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(request)
  })
    .then((response) => response.json())
    .then((data) => {
      if (data.success == true) {
        fetch("/reset", {
          method: "POST",
        })
          .then((response) => {
            if (response.ok) {
              document.getElementById("message").textContent = "";
            } else {
              document.getElementById("message").textContent =
                "Ocurrió un error al resetear el contador";
            }
          })
          .catch((error) => {
            console.error(error);
          });
      } else {
        document.getElementById("message").textContent =
          "*Usuario o contraseña incorrectos";
      }
    })
    .catch((error) => {
      console.error(error);
    });
  } 


// Function to update the table with the new data
function updateTable() {
  // Make an AJAX request to the Flask route that returns the updated data
  fetch("/update_data")
    .then((response) => response.json())
    .then((data) => {
      // Update the table with the new data
      if(cameraCount === 1){
        document.getElementById("table-body").innerHTML = `
      <tr>
          <td>${data.fecha}</td>
          <td>${data.contador}</td>
          <td>${data.evento}</td>
          <td>${data.subidas}</td>
          <td>${data.bajadas}</td>
      </tr> 
      `;
      } else if(cameraCount === 2){
        document.getElementById("table-body").innerHTML = `
        <tr>
            <td>${data.fecha}</td>
            <td>${data.contador}</td>
            <td>${data.evento}</td>
            <td>${data.subidas}</td>
            <td>${data.bajadas}</td>
        </tr> 
        <tr>
            <td>${data.fecha_back}</td>
            <td>${data.contador_back}</td>
            <td>${data.evento_back}</td>
            <td>${data.subidas_back}</td>
            <td>${data.bajadas_back}</td>
        </tr> 
        `;
      }
    })
    .catch((error) => console.error(error));
}

// Call the updateTable function every 5 seconds
setInterval(updateTable, 5000);

const targetHour = 0;
const targetMinute = 0;

// resetFunction to be executed at the desired time
function resetFunction() {
  fetch("/reset", {
    method: "POST",
  })
    .then((response) => {
      if (response.ok) {
        document.getElementById("message").textContent = "";
      } else {
        document.getElementById("message").textContent =
          "*Usuario o contraseña incorrectos";
      }
    })
    .catch((error) => {
      console.error(error);
    });
}

// Call the function every minute to check if the desired time has been reached and execute the resetFunction
setInterval(function () {
  const now = new Date();
  const currentHour = now.getHours();
  const currentMinute = now.getMinutes();
  if (currentHour === targetHour && currentMinute === targetMinute) {
    resetFunction();
  }
}, 60 * 1000); // Repeat every minute
