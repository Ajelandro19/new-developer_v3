let cameraCount;

        // Use Fetch API to make an AJAX request to the Flask API endpoint
        fetch('/cameras_number')
            .then(response => response.json())
            .then(data => {
                // Access the camera count from the JSON response
                cameraCount = parseInt(data);
                // Now, you can use the cameraCount variable in other scripts
                // For example, you can log it to the console
                if (cameraCount == 1) {
                  var back_button = document.getElementById("back_button");
                  back_button.style.display = "none";
                }
            })
            .catch(error => {
                console.error('Error fetching camera count:', error);
            });


function postData(
  x1,
  y1,
  x2,
  y2,
  ux1,
  uy1,
  ux2,
  uy2,
  dx1,
  dy1,
  dx2,
  dy2,
  ax1,
  ay1,
  ax2,
  ay2,
  sensitivity
) {
  const data = {
    id: "front",
    roi_x1: x1,
    roi_y1: y1,
    roi_x2: x2,
    roi_y2: y2,
    up_x1: ux1,
    up_y1: uy1,
    up_x2: ux2,
    up_y2: uy2,
    down_x1: dx1,
    down_y1: dy1,
    down_x2: dx2,
    down_y2: dy2,
    area_x1: ax1,
    area_y1: ay1,
    area_x2: ax2,
    area_y2: ay2,
    sensitivity: sensitivity,
  };
  const jsonData = JSON.stringify(data);
  $.ajax({
    type: "PUT",
    url: "/settings",
    contentType: "application/json",
    data: jsonData,
    success: function () {
      //console.log("ROI sent successfully ");
      if (confirm("Desea regresar a la pagina principal?"))
        window.location.replace("process");
    },
    error: function () {
    },
  });
}

function resetVideo() {
  if (confirm("Está seguro de que desea reiniciar el video?")) {
    // Get the selected radio button value
    postData(
      72,
      53,
      540,
      417,
      102,
      92,
      300,
      399,
      357,
      94,
      505,
      394,
      0,
      0,
      640,
      480,
      100
    );
  } else {
  }
}

function setupCanvas() {
  var mouse = { x: 0, y: 0 };
  var isMouseDown = false;
  var x1, y1, x2, y2;
  var isArea = false;
  var isROI = true;
  var isUp = false;
  var isDown = false;
  // Set up canvas for ROI selection
  var canvas = document.getElementById("canvas");
  var ctx = canvas.getContext("2d");
  var img = new Image();
  var lastArea = null;
  var lastROI = null;
  var lastUp = null;
  var lastDown = null;
  var area = null;
  var ROI = null;
  var Up = null;
  var Down = null;
  var slider = document.getElementById("mySlider");
  var output = document.getElementById("sliderValue");

  img.onload = function () {
    canvas.style.backgroundImage = "url('" + img.src + "')";
    canvas.width = img.width;
    canvas.height = img.height;
    startX = 0;
    startY = 0;
    width = img.width;
    height = img.height;
    fetch("settings/front")
      .then((response) => response.json())
      .then((data) => {
        lastArea = {
          startX: Math.min(data.area[0], data.area[2]),
          startY: Math.min(data.area[1], data.area[3]),
          width: Math.abs(data.area[2] - data.area[0]),
          height: Math.abs(data.area[3] - data.area[1]),
        };
        lastROI = {
          startX: Math.min(data.roi[0], data.roi[2]),
          startY: Math.min(data.roi[1], data.roi[3]),
          width: Math.abs(data.roi[2] - data.roi[0]),
          height: Math.abs(data.roi[3] - data.roi[1]),
        };
        lastUp = {
          startX: Math.min(data.up[0], data.up[2]),
          startY: Math.min(data.up[1], data.up[3]),
          width: Math.abs(data.up[2] - data.up[0]),
          height: Math.abs(data.up[3] - data.up[1]),
        };
        lastDown = {
          startX: Math.min(data.down[0], data.down[2]),
          startY: Math.min(data.down[1], data.down[3]),
          width: Math.abs(data.down[2] - data.down[0]),
          height: Math.abs(data.down[3] - data.down[1]),
        };
        slider.value = data.sensitivity;
        output.textContent = slider.value;
        drawRectangles(lastArea, lastROI, lastUp, lastDown);
      })
      .catch((error) => {
        // Handle any errors
        lastArea = null;
        lastROI = null;
        lastUp = null;
        lastDown = null;
        console.error("Error:", error);
      });
  };

  img.src = "cam";
  canvas.addEventListener("mousedown", function (e) {
    var rect = canvas.getBoundingClientRect();
    mouse.x = e.clientX - rect.left;
    mouse.y = e.clientY - rect.top;
    isMouseDown = true;

    // Update the initial values
    startX = mouse.x;
    startY = mouse.y;
    width = 0;
    height = 0;
  });

  canvas.addEventListener("mousemove", function (e) {
    if (!isMouseDown) return;
    if (isROI) lastROI = null;
    else if (isArea) lastArea = null;
    else if (isUp) lastUp = null;
    else if (isDown) lastDown = null;
    var rect = this.getBoundingClientRect();
    var x = e.clientX - rect.left;
    var y = e.clientY - rect.top;

    // Perform bounds checking
    x = Math.max(0, Math.min(x, img.width));
    y = Math.max(0, Math.min(y, img.height));

    // Update the mouse coordinates
    mouse.x = x;
    mouse.y = y;

    ctx.clearRect(0, 0, canvas.width, canvas.height);
    startX = Math.min(mouse.x, startX);
    startY = Math.min(mouse.y, startY);
    width = Math.abs(mouse.x - startX);
    height = Math.abs(mouse.y - startY);

    // Draw the rectangle
    drawRectangles(lastArea, lastROI, lastUp, lastDown);

    ctx.beginPath();
    ctx.lineWidth = 2;
    if (isROI) {
      ctx.strokeStyle = "rgb(0, 255, 0)";
      ctx.rect(startX, startY, width, height);
    } else if (isArea) {
      ctx.strokeStyle = "rgb(255, 0, 0)";
      ctx.moveTo(startX, startY);
      ctx.rect(startX, startY, width, height);
    } else if (isUp) {
      ctx.strokeStyle = "rgb(255, 0, 255)";
      ctx.moveTo(startX, startY);
      ctx.rect(startX, startY, width, height);
    } else if (isDown) {
      ctx.strokeStyle = "rgb(0, 0, 255)";
      ctx.moveTo(startX, startY);
      ctx.rect(startX, startY, width, height);
    }
    ctx.stroke();
    ctx.closePath();
  });

  canvas.addEventListener("mouseup", function (e) {
    isMouseDown = false;
    if (isArea) {
      lastArea = {
        startX: startX,
        startY: startY,
        width: width,
        height: height,
      };
    } else if (isROI) {
      lastROI = {
        startX: startX,
        startY: startY,
        width: width,
        height: height,
      };
    } else if (isUp) {
      lastUp = {
        startX: startX,
        startY: startY,
        width: width,
        height: height,
      };
    } else if (isDown) {
      lastDown = {
        startX: startX,
        startY: startY,
        width: width,
        height: height,
      };
    }

    var rect = canvas.getBoundingClientRect();
    x1 = Math.max(0, Math.min(startX, e.clientX - rect.left));
    y1 = Math.max(0, Math.min(startY, e.clientY - rect.top));
    x2 = Math.min(img.width, Math.max(startX + width, e.clientX - rect.left));
    y2 = Math.min(img.height, Math.max(startY + height, e.clientY - rect.top));

    // Calculate the conversion factor between canvas and video frame coordinates
    var factor_x = img.width / canvas.offsetWidth;
    var factor_y = img.height / canvas.offsetHeight;
    // Convert the ROI coordinates to video frame coordinates
    x1 = Math.round(x1 * factor_x);
    y1 = Math.round(y1 * factor_y);
    x2 = Math.round(x2 * factor_x);
    y2 = Math.round(y2 * factor_y);

    if (isArea) {
      area = {
        x1: x1,
        y1: y1,
        x2: x2,
        y2: y2,
      };
    } else if (isROI) {
      ROI = {
        x1: x1,
        y1: y1,
        x2: x2,
        y2: y2,
      };
    } else if (isUp) {
      Up = {
        x1: x1,
        y1: y1,
        x2: x2,
        y2: y2,
      };
    } else {
      Down = {
        x1: x1,
        y1: y1,
        x2: x2,
        y2: y2,
      };
    }
  });

  var roiAreaButton = document.getElementById("roi_area");
  var roiButton = document.getElementById("roi");
  var upButton = document.getElementById("up");
  var downButton = document.getElementById("down");
  roiButton.addEventListener("click", function () {
    isROI = true;
    isUp = false;
    isDown = false;
    isArea = false;
  });
  upButton.addEventListener("click", function () {
    isROI = false;
    isUp = true;
    isDown = false;
    isArea = false;
  });
  downButton.addEventListener("click", function () {
    isROI = false;
    isUp = false;
    isDown = true;
    isArea = false;
  });
  roiAreaButton.addEventListener("click", function () {
    isROI = false;
    isUp = false;
    isDown = false;
    isArea = true;
  });

  // Display the initial value
  output.textContent = slider.value;

  // Update the displayed value as the slider value changes
  slider.addEventListener("input", function () {
    output.textContent = slider.value;
  });
  const button = document.getElementById("save");
  // Add event listener to the button
  button.addEventListener("click", async function () {
    const response = await fetch("settings/front");
    const data = await response.json();

    if (slider.value == data.sensitivity) {
      output.textContent = data.sensitivity;
    }

    if (ROI == null) {
      ROI = {
        x1: data.roi[0],
        y1: data.roi[1],
        x2: data.roi[2],
        y2: data.roi[3],
      };
    }

    if (Up == null) {
      Up = {
        x1: data.up[0],
        y1: data.up[1],
        x2: data.up[2],
        y2: data.up[3],
      };
    }

    if (Down == null) {
      Down = {
        x1: data.down[0],
        y1: data.down[1],
        x2: data.down[2],
        y2: data.down[3],
      };
    }

    if (area == null) {
      area = {
        x1: data.area[0],
        y1: data.area[1],
        x2: data.area[2],
        y2: data.area[3],
      };
    }
    // Code to be executed when the button is clicked
    postData(
      ROI.x1,
      ROI.y1,
      ROI.x2,
      ROI.y2,
      Up.x1,
      Up.y1,
      Up.x2,
      Up.y2,
      Down.x1,
      Down.y1,
      Down.x2,
      Down.y2,
      area.x1,
      area.y1,
      area.x2,
      area.y2,
      output.textContent
    );
  });

  drawRectangles = function (lastArea, lastROI, lastUp, lastDown) {
    // Draw the rectangle
    if (lastROI) {
      ctx.beginPath();
      ctx.lineWidth = 2;
      ctx.strokeStyle = "rgb(0, 255, 0)";
      ctx.rect(lastROI.startX, lastROI.startY, lastROI.width, lastROI.height);
      ctx.stroke();
      ctx.closePath();
    }
    // Draw Area rectangle
    if (lastArea) {
      ctx.beginPath();
      ctx.lineWidth = 2;
      ctx.strokeStyle = "rgb(255, 0, 0)";
      ctx.rect(
        lastArea.startX,
        lastArea.startY,
        lastArea.width,
        lastArea.height
      );
      ctx.stroke();
      ctx.closePath();
    }

    // Draw Up rectangle
    if (lastUp) {
      ctx.beginPath();
      ctx.lineWidth = 2;
      ctx.strokeStyle = "rgb(255, 0, 255)";
      ctx.moveTo(lastUp.startX, lastUp.startY);
      ctx.rect(lastUp.startX, lastUp.startY, lastUp.width, lastUp.height);
      ctx.stroke();
      ctx.closePath();
    }
    // Draw Down rectangle
    if (lastDown) {
      ctx.beginPath();
      ctx.lineWidth = 2;
      ctx.strokeStyle = "rgb(0, 0, 255)";
      ctx.moveTo(lastDown.startX, lastDown.startY);
      ctx.rect(
        lastDown.startX,
        lastDown.startY,
        lastDown.width,
        lastDown.height
      );
      ctx.stroke();
      ctx.closePath();
    }
  };
}

$(document).ready(function () {
  setupCanvas();
});
