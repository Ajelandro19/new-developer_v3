// Showing boxes to fill in the IP camera information
function showForm() {
  var selectedOption = document.getElementById("camara").value;
  var form1= document.getElementById("ipcam-form");
  var form2= document.getElementById("ipcam-form2");
  var button = document.getElementById("Monitor");
  if (selectedOption === "1") {
    form1.style.display = "block";
    form2.style.display = "none";
    button.style.display = "inline-block";
    button.style.marginLeft = "50%";
    button.style.alignItems = "center";
  } else if (selectedOption === "2") {
    form1.style.display = "none";
    form2.style.display = "block";
    button.style.display = "inline-block";
    button.style.marginLeft = "50%";
    button.style.alignItems = "center";
  } else {
    form1.style.display = "none";
    form2.style.display = "none";
    button.style.display = "none";
  }
}


function submitForms(buttonClicked) {
  var doubleForm = document.getElementById("double");
  var singleForm = document.getElementById("ip");
  var cameraForm = document.getElementById("cam-form");
  var button = document.getElementById("Monitor");
  var loadingOverlay = document.getElementById("loadingOverlay");

  var isDoubleFormValid = doubleForm.checkValidity();
  var isSingleFormFormValid = singleForm.checkValidity();
  var a = buttonClicked;

  // if (cameraForm.elements["camt"].value == "1") {
  // // create a new FormData object and append the form data
  //   var formData = new FormData();
  //   formData.append("ipcam", singleForm.elements["ipcam"].value);
  //   formData.append("port", singleForm.elements["port"].value);
  //   formData.append("user", singleForm.elements["user"].value);
  //   formData.append("password", singleForm.elements["password"].value);
  //   formData.append("button", a);
  // } else if (cameraForm.elements["camt"].value == "2") {
  //   var formData = new FormData();
  //     formData.append("front_ipcam", doubleForm.elements["front_ipcam"].value);
  //     formData.append("front_port", doubleForm.elements["front_port"].value);
  //     formData.append("front_user", doubleForm.elements["front_user"].value);
  //     formData.append("front_password", doubleForm.elements["front_password"].value);
  //     formData.append("back_ipcam", doubleForm.elements["back_ipcam"].value);
  //     formData.append("back_port", doubleForm.elements["back_port"].value);
  //     formData.append("back_user", doubleForm.elements["back_user"].value);
  //     formData.append("back_password", doubleForm.elements["back_password"].value);
  //     formData.append("button", a);
  // }

  var formData = {}; // Create an object to hold the data
  
  // ... Other code ...
  
  if (cameraForm.elements["camt"].value == "1") {
    formData = {
      ipcam: singleForm.elements["ipcam"].value,
      port: singleForm.elements["port"].value,
      user: singleForm.elements["user"].value,
      password: singleForm.elements["password"].value,
      button: a
    };
  } else if (cameraForm.elements["camt"].value == "2") {
    formData = {
      front_ipcam: doubleForm.elements["front_ipcam"].value,
      front_port: doubleForm.elements["front_port"].value,
      front_user: doubleForm.elements["front_user"].value,
      front_password: doubleForm.elements["front_password"].value,
      back_ipcam: doubleForm.elements["back_ipcam"].value,
      back_port: doubleForm.elements["back_port"].value,
      back_user: doubleForm.elements["back_user"].value,
      back_password: doubleForm.elements["back_password"].value,
      button: a
    };
  }
  var jsonData = JSON.stringify(formData);
  if (isDoubleFormValid) var url = "/submit-form/double";
  else if (isSingleFormFormValid) var url = "/submit-form/single";
  // send an AJAX request to the server with the form data
  var xhr = new XMLHttpRequest();
  xhr.open("POST", url);
  xhr.setRequestHeader("Content-Type", "application/json"); // Set the content type to JSON
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
      if (xhr.status === 200) {
        console.log("");
        // handle the response from the server
      } else {
        console.error("Error:");
      }
    }
  };
  if (!isSingleFormFormValid && !isDoubleFormValid) {
    alert("Por favor, llene todos los campos antes de continuar.");
    return;
  } else if (isSingleFormFormValid || isDoubleFormValid) {
    button.disabled= true;
    xhr.send(jsonData);
    alert("La aplicación se reniciará para aplicar los cambios.")
    loadingOverlay.classList.add("active");
    setTimeout(function () {
      window.location.assign("process");
      loadingOverlay.classList.remove("active");
    }, 25000);
    button.addEventListener("click", handleClick);
  }
}
