document.addEventListener("DOMContentLoaded", function () {
    const configContainer = document.getElementsByClassName("content-container");
    const credentialsDiv = document.getElementById("credentials");
    const routeDiv = document.getElementById("route");
    const submitButton = document.getElementById("submit-button");
    const routeButton = document.getElementById("submit-route");
    submitButton.addEventListener("click", function () {
        const form = document.getElementById("credentials-form");
        const formData = new FormData(form);
        const formDataObject = {};
        var isCredentiasFormValid = form.checkValidity();

        if (!isCredentiasFormValid) {
            alert("Por favor, complete todos los campos.");
            return;
        }
        formData.forEach((value, key) => {
            formDataObject[key] = value;
        });
        fetch("/set_credentials", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(formDataObject),
        })
        .then(response => response.json())
        .then(data => {
        })
        .catch(error => {
            alert("Ocurrió un error, actualice y vuelva a intentar")
        });
        credentialsDiv.classList.remove("show");
        credentialsDiv.classList.add("hidden");
        routeDiv.classList.remove("hidden");
        configContainer.height = "500px";
        routeDiv.classList.add("show");
        const div = document.getElementById("configurationBox");
        div.classList.add("expanded");
        routeDiv.style.height = "auto";
        setTimeout(function () {
            credentialsDiv.style.display = "none";
        }, 500);
    });

    routeButton.addEventListener("click", function () {
        const form = document.getElementById("route-form");
        const formData = new FormData(form);
        const formDataObject = {};
        var isRouteFormValid = form.checkValidity();

        if (!isRouteFormValid) {
            alert("Por favor, complete todos los campos.");
            return;
        }
        formData.forEach((value, key) => {
            formDataObject[key] = value;
        });
        fetch("/device_settings", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(formDataObject),
        })
        .then(response => response.json())
        .then(data => {
        })
        .catch(error => {
        });
        routeDiv.classList.remove("show");
        routeDiv.classList.add("hidden");
        window.location.assign("ip_settings");
    });
});