import base64
class ConvertText:
    def __init__(self, text):
        self.text = text

    def hex_to_string(self):
        try:
            string_value = bytes.fromhex(self.text).decode('ISO-8859-1')
            return string_value
        except ValueError:
            return None
    
    def base64_to_string(self):
        try:
            base64_bytes = self.text.encode('ascii')
            message_bytes = base64.b64decode(base64_bytes)
            message = message_bytes.decode('utf-8')
            return message
        except (ValueError, UnicodeDecodeError):
            return None

    def detect_and_convert(self):
        result = self.hex_to_string()
        if result is not None:
            return result
        result = self.base64_to_string()
        if result is not None:
            return result
        return "Formato desconocido"