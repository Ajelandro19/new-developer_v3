class MatchingPair:
    def __init__(self, numbers, target_sum):
        self.numbers = numbers
        self.target_sum = target_sum

    def find_pair(self):
        left = 0
        right = len(self.numbers) - 1

        while left < right:
            current_sum = self.numbers[left] + self.numbers[right]
            if current_sum == self.target_sum:
                return (self.numbers[left], self.numbers[right])
            elif current_sum < self.target_sum:
                left += 1
            else:
                right -= 1
        return None