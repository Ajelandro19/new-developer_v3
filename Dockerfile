FROM python:3.10-slim-buster

# Instalar Nginx
RUN apt-get update && apt-get install -y nginx

# Establecer un directorio de trabajo
WORKDIR /app

# Copiar los archivos de requisitos e instalar las dependencias
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

# Establecer path
ENV PYTHONPATH=/app

# Copiar el resto del código de la aplicación
COPY . .

#Ejecutar pytest
RUN pytest

# Copiar el archivo de configuración de Nginx
COPY nginx.conf /etc/nginx/sites-enabled/default

# Exponer el puerto en el que se ejecutará Nginx
EXPOSE 80

# Comando para ejecutar Nginx y la aplicación
CMD service nginx start && python app.py
